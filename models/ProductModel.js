const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({ 
    name: {type: String, required: true, unique: true, trim:true},
    price: {type: Number, required: true, trim:true},
    color: {type: String, required: true, trim:true},
    description: {type: String, required: true, unique: true, trim:true},
    //categoryID: {type: Schema.Types.ObjectId, required: false, trim:true} 
});


module.exports =  mongoose.model('product', productSchema);