const Product = require('../models/ProductModel');

class ProductController {
    // GET FIND ALL
    async findAll(req, res){
        try{
            const product = await Product.find({});
            res.send(product);
        }
        catch(e){
            res.send({e})
        }
    }
    // FIND ONE PRODUCT BY _ID
    async findOne(req ,res){
        let { product_id} = req.params;
        try{
            const product = await Product.findOne({_id:product_id});
            res.send(product);
        }
        catch(e){
            res.send({e})
        }

    }
    // POST ADD ONE
    async insert (req, res) {
        let { name, price, color, description} = req.body;
        try{
            const addProduct = await Product.create({
                name:name,
                price:price,
                color:color,
                description:description,
                
            });
            res.send(addProduct)
        }
        catch(e){
            res.send({e})
        }
    }
    // DELETE PRODUCT
    async delete (req, res){
        console.log('delete!!!')
        let { _id } = req.body;
        try{
            const removed = await Product.deleteOne({ _id });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }
    // UPDATE PRODUCT

    async update (req, res){
        let { _id, name, price, color, description, newName, newColor, newPrice, newDes} = req.body;
        try{
            const updated = await Product.updateOne(
                { _id, name, price, color, description},{
                    name:newName,
                    price: newPrice,
                    color:newColor,
                    description:newDes,
                }
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }


};
module.exports = new ProductController();