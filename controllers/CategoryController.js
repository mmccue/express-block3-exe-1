const Category = require('../models/CategoryModel');

class CategoryController {
    // GET FIND ALL
    async findAll(req, res){
        try{
            const category = await Category.find({});
            res.send(category);
        }
        catch(e){
            res.send({e})
        }
    }
    // FIND ONE CATEGORY BY _ID
    async findOne(req ,res){
        let { category_id} = req.params;
        try{
            const category = await Category.findOne({_id:category_id});
            res.send(category);
        }
        catch(e){
            res.send({e})
        }

    }
    // POST ADD ONE
    async insert (req, res) {
        let { category } = req.body;
        try{
            const done = await Category.create({category});
            res.send(done)
        }
        catch(e){
            res.send({e})
        }
    }
    // DELETE CATEGORY
    async delete (req, res){
        console.log('delete!!!')
        let { category } = req.body;
        try{
            const removed = await Category.deleteOne({ category });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }
    // UPDATE CATEGORY

    async update (req, res){
        let { category, newCategory } = req.body;
        try{
            const updated = await Category.updateOne(
                { category },{ category:newCategory }
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }


};
module.exports = new CategoryController();