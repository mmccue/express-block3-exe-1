const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/ProductController');

//  == This route will give us back all products: ==  //

router.get('/', controller.findAll);

//  == This route will give us back one product, it will be that with the id we are providing: ==  //

router.get('/:product_id', controller.findOne);

//  == This route allow us to add an extra product: ==  //

router.post('/new', controller.insert);

//  == This route allow us to delete one product it will be that with the id we are providing: ==  //

router.post('/delete', controller.delete);

//  == This route allow us to update one product it will be that with the id we are providing ==  //

router.post('/update', controller.update);

module.exports = router;