const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/CategoryController');

//  == This route will give us back all cateogries: ==  //

router.get('/', controller.findAll);

//  == This route will give us back one category, it will be that with the id we are providing: ==  //

router.get('/:category_id', controller.findOne);

//  == This route allow us to add an extra cateogry: ==  //

router.post('/new', controller.insert);

//  == This route allow us to delete one cateogry it will be that with the id we are providing: ==  //

router.post('/delete', controller.delete);

//  == This route allow us to update one category it will be that with the id we are providing ==  //

router.post('/update', controller.update);

module.exports = router;